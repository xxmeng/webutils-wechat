# -*- coding: utf-8 -*-


class FrontendKeys(object):
    # string, args: user_type; user.id_ expiration: user token's expiration, msgpack encoded
    user = 'user:{}:{}'


class WechatAppKeys(object):
    # string
    access_token = 'access_token'
