# -*- coding: utf-8 -*-

from .base import BaseAppException
from .auth import AuthException
from .utils import UtilsException
